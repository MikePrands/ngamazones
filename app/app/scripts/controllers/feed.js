'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('FeedCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
