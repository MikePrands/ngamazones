'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:AuthCtrl
 * @description
 * # AuthCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('AuthCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
