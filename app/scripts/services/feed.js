'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.Feed
 * @description
 * # Feed
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Feed',
    ['$resource', 'TheEnvironment',
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/feed');
 }]);