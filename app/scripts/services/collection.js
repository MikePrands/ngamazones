'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.collection
 * @description
 * # collection
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Collection', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/users/:user_slug/collections/:collection_slug');
  }]);
