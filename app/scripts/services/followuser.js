'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.FollowUser
 * @description
 * # FollowUser
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('FollowUser', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/users/:user_slug/follow', {user_slug:'@user_slug'},
      {
        'follow': { method: 'PUT'}
      });
 }]);

