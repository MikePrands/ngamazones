'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.categories
 * @description
 * # categories
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Categories', 
  	['$resource', 'TheEnvironment', 
  	function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/categories/');
}]);
