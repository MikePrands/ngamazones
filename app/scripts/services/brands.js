'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.brands
 * @description
 * # brands
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Brands', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/manufacturers');
}]);
