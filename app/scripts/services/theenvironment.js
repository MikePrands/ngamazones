'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.TheEnvironment
 * @description
 * # TheEnvironment
 * Factory in the theamazonesApp.
 */

angular.module('theamazonesApp')
  .factory('TheEnvironment',
    function(isLocal, isStaging, isProduction, localPath, stagingPath, productionPath, localApiPathUrl, stagingApiPathUrl, productionApiPathUrl){
      
      var checkEnvironment = {
        'local': isLocal,
        'staging': isStaging,
        'production': isProduction
      },
      environment,
      currentPath,
      currentApiUrl;

      if(checkEnvironment.local){
        currentPath = localPath;
        currentApiUrl = localApiPathUrl;
      } else if(checkEnvironment.staging) {
        currentPath = stagingPath;
        currentApiUrl = stagingApiPathUrl;
      } else if(checkEnvironment.production) {
        currentPath = productionPath;
        currentApiUrl = productionApiPathUrl;
      }

      environment = {
        'path': currentPath,
        'apiUrl': currentApiUrl
      };
      // console.info('environment: ' + environment.apiUrl);
      return environment;

  }); 