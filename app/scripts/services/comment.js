'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.Comment
 * @description
 * # Comment
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
.factory('Comment', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/products/:product_slug/comments', {product_slug:'@product_slug', text:'@text', user_slug:'@user_slug'},
      {
        'comment': { method: 'POST'}
      });
 }]);
