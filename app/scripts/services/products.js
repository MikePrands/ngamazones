'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.ListaProdotti
 * @description
 * # ListaProdotti
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Products', 
  	['$resource', 'TheEnvironment',
  	function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/products');
    	// return $resource('http://theamazones-env-qrjkmdrtzp.elasticbeanstalk.com/products');
    	// return $resource('http://localhost:9000/api/products.json');
  }]);
