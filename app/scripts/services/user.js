'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.User
 * @description
 * # User
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('User', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/users/:user_slug');
 }]);
