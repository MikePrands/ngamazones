'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.brand
 * @description
 * # brand
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Brand',
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/manufacturers/:brand_slug');
}]);
