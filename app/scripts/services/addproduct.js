'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.AddProduct
 * @description
 * # AddProduct
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('AddProduct', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/products/', {url:'@url'});
    
  }]);
