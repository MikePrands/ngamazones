'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.RemoveItemCollection
 * @description
 * # RemoveItemCollection
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('RemoveItemCollection', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/users/:user_slug/collections/:collection_slug/items/:item_id');
  }]);
