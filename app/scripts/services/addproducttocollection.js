'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.AddProductToCollection
 * @description
 * # AddProductToCollection
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('AddProductToCollection', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/users/:user_slug/collections/:collection_slug/items', 
        {
          user_slug:        '@user_slug', 
          collection_slug:  '@collection_slug', 
          product_id:       '@product_slug'
        },
        {
          'addProduct': { method: 'POST'}
        });
  }]);
