'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.AddUserCollection
 * @description
 * # AddUserCollection
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('AddUserCollection', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/users/:user_slug/collections/', {user_slug:'@user_slug', name:'@name'},
      {
        'addCollection': { method: 'POST'}
      });
  }]);