'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.UserCollections
 * @description
 * # UserCollections
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('UserCollections', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
      return $resource(TheEnvironment.apiUrl + '/users/:user_slug/collections');
  }]);
