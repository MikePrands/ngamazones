'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.product
 * @description
 * # product
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Product', 
  	['$resource', 'TheEnvironment',
  	function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/products/:product_slug', {product_slug:'@product_slug'},
    		{
    			'getProduct': { method: 'GET', isArray:false}
    		}
    	);
 }]);
