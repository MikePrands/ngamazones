'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.MetaInfo
 * @description
 * # MetaInfo
 * Service in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('MetaInfo', function () {

    var metaDescription = '',
      	metaKeywords = '',
      	ogTitle = 'TheAmazones!',
      	ogImage = '',
      	ogType = '',
      	ogUrl = '',
      	ogDescription = '',
      	title = 'TheAmazones!';

      return {
      	title: function() { return title; },
        metaDescription: function() { return metaDescription; },
        metaKeywords: function() { return metaKeywords; },

        setTitle: function(newTitle) { 
        	title = newTitle; 
        },
        reset: function() {
          metaDescription = '';
          metaKeywords = '';
        },
        setMetaDescription: function(newMetaDescription) {
          metaDescription = newMetaDescription;
        },
        appendMetaKeywords: function(newKeywords) {
          for (var key in newKeywords) {
            if (metaKeywords === '') {
              metaKeywords += newKeywords[key].name;
            } else {
              metaKeywords += ', ' + newKeywords[key].name;
            }
          }
        }
      };
      
  });
