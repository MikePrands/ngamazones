'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.ListaCollezioni
 * @description
 * # ListaCollezioni
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Collections', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/collections');
  }]);
