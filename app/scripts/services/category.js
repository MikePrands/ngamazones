'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.category
 * @description
 * # category
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Category', 
  	['$resource', 'TheEnvironment', 
  	function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/categories/:category_slug');
}]);