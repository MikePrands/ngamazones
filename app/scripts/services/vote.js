'use strict';

/**
 * @ngdoc service
 * @name theamazonesApp.vote
 * @description
 * # vote
 * Factory in the theamazonesApp.
 */
angular.module('theamazonesApp')
  .factory('Vote', 
    ['$resource', 'TheEnvironment', 
    function ($resource, TheEnvironment) {
    	return $resource(TheEnvironment.apiUrl + '/products/:product_slug/vote', {product_slug:'@product_slug'},
      {
        'vote': { method: 'PUT'}
      });
 }]);
