(function() {
  var ROOT_URL = "http://localhost:9000";

  var PRICE_SELECTORS = [
    ".priceLarge",
    ".a-color-price.a-size-large",
    ".a-color-price.a-size-medium",
    "#priceblock_ourprice"
  ];

  var IMAGE_SELECTORS = [
    "#detailImg",
    "#main-image",
    "#prodImage",
    "#original-main-image",
    "#landingImage",
  ];

  var TITLE_SELECTORS = [
    "#btAsinTitle",
    "#title_feature_div #title",
  ];

  var srcById = function(id) {
    var img = document.getElementById(id);
    return img && img.src;
  }

  var innerTextById = function(id) {
    var div = document.getElementById(id);
    return div && div.textContent;
  }

  var innerTextByQS = function(qs) {
    var div = document.querySelector(qs);
    return div && div.textContent;
  }

  var findBySelectors = function(selectors) {
    var val = null;
    for (sel in selectors) {
      val = val || innerTextByQS(selectors[sel]);
    }
    return val;
  }

  var srcBySelectors = function(selectors) {
    var val = null;
    for (sel in selectors) {
      var img = document.querySelector(selectors[sel]);
      val = val || (img && img.src);
    }
    return val;
  }

  var imageByDataAttr = function() {
    var el = document.querySelector("[data-old-hires]");
    if (el) {
      return el.getAttribute("data-old-hires");
    }
    return null;
  }

  var urlParts = window.location.href.split('/');
  var asinIndex = urlParts.indexOf('dp') + 1;
  if (asinIndex == 0) asinIndex = urlParts.indexOf('product') + 1;
  if (asinIndex == 0) asinIndex = urlParts.indexOf('ASIN') + 1;
  var asin;
  if (asinIndex > 0)
    asin = urlParts[asinIndex];
  var image = imageByDataAttr() || srcBySelectors(IMAGE_SELECTORS);
  var title = findBySelectors(TITLE_SELECTORS);
  if (title) title = title.substring(0, 125);
  var price = findBySelectors(PRICE_SELECTORS);
  var link = window.location.href;

  var product = {asin:asin, image:image, name: title, price:price};
  var frame = document.createElement('iframe');

  var serialize = function(obj) {
    var str = [];
    for(var p in obj)
       str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    return str.join("&");
  }
  document.body.appendChild(frame);
  var url;
  if (product.asin) {
    url = ROOT_URL + "/extension/popup?" + serialize(product);
  } else {
    url = ROOT_URL + "/extension/not_found";
  }
  frame.src = url;
  frame.style.position = 'fixed'
  frame.style.border = 0;
  frame.style.boxShadow = "0 0 40px hsla(0, 0%, 0%, .4)";
  frame.style.top = '20%';
  frame.style.left = '50%';
  frame.style.marginLeft = "-150px";
  frame.style.zIndex = 9001;
  frame.style.height = "383px";
  frame.style.background = 'white';

  var closeButton = document.createElement("div")
  closeButton.style.position = "fixed";
  closeButton.style.top = '0';
  closeButton.style.left = '0';
  closeButton.style.width = "100%";
  closeButton.style.height = "100%";
  closeButton.style.zIndex = 9000;
  closeButton.style.opacity = ".65";
  closeButton.style.backgroundColor = "black";
  closeButton.addEventListener("click", function() {
    frame.remove()
    closeButton.remove()
  })
  document.body.appendChild(closeButton);

})();