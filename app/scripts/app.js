'use strict';

/**
 * @ngdoc overview
 * @name theamazonesApp
 * @description
 * # theamazonesApp
 *
 * Main module of the application.
 */

// var mainUrl = 'http://localhost:9000';

var theAmazones = angular.module('theamazonesApp', [
    'ngCookies',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngResource',
    'ng-token-auth',
    'angularMoment',
    'angularFileUpload',
    'angulartics', 
    'angulartics.google.analytics'
  ]);
  
  theAmazones
    .constant('isLocal', true)                                        // is local environment
    .constant('isStaging', false)                                     // is staging environment
    .constant('isProduction', false)                                  // is production environment
    .constant('localPath', 'http://localhost:9000')                   // the local path 
    .constant('stagingPath', 'http://theamazones-static.s3-website.eu-central-1.amazonaws.com/')                   // the staging path
    .constant('productionPath', 'http://sceltiamano.it')              // the production path
    .constant('localApiPathUrl', 'http://localhost:3000')             // the local API path
    .constant('stagingApiPathUrl', 'http://theamazones-env-qrjkmdrtzp.elasticbeanstalk.com')        // the staging API path
    .constant('productionApiPathUrl', 'http://sceltiamano:3000');     // the production API path

  theAmazones.config(
    ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$authProvider', '$compileProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider, $authProvider, $compileProvider) {

      // IF production ready set it to FALSE
      // $compileProvider.debugInfoEnabled(false);

    var urlApi;

    // IF LOCAL
    urlApi = 'http://localhost:3000';
    // IF STAGING
    // urlApi = 'http://theamazones-env-qrjkmdrtzp.elasticbeanstalk.com';
    // IF PRODUCTION
    // urlApi = 'http://sceltiamano:3000';

    $authProvider.configure({
        apiUrl: urlApi,
        confirmationSuccessUrl: window.location.href,
        authProviderPaths: {
          twitter:   '/auth/twitter',
          facebook:  '/auth/facebook'
        },
        passwordResetSuccessUrl: '/modifica-password'
    });

    // $authProvider.configure({
    //     apiUrl: 'http://localhost:3000',
    //     confirmationSuccessUrl: window.location.href,
    //     authProviderPaths: {
    //       twitter:   '/auth/twitter',
    //       facebook:  '/auth/facebook'
    //     },
    // });

  	$urlRouterProvider.otherwise('/');

  	$stateProvider
  	// HOMEPAGE
      .state('homepage', {
        url: '/',
        templateUrl: 'views/homepage.html',
        controller: 'HomepageCtrl'
      })
      .state('homepagePaginated', {
        url: '/pagina/:page',
        templateUrl: 'views/homepage.html',
        controller: 'HomepageCtrl',
      })
    // RICERCA
      .state('search', {
        url: '/cerca/?q',
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .state('searchPaginated', {
        url: '/cerca/pagina/:page/?:q/',
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl',
      })
    // PRODOTTO SINGOLO NEW
      .state('product', {
        url: '/prodotto/:product_slug',
        templateUrl: 'views/product.html',
        controller: 'SingleProductCtrl'
      })
    // CATEGORIE
      .state('categories', {
        url: '/categorie',
        templateUrl: 'views/categories.html',
        controller: 'CategoriesCtrl'
      })
     // SINGOLA CATEGORIA
      .state('category', {
        url: '/categorie/:category_slug',
        templateUrl: 'views/category.html',
        controller: 'CategoryCtrl'
      })
      .state('categoryPaginated', {
        url: '/categorie/:category_slug/pagina/:page',
        templateUrl: 'views/category.html',
        controller: 'CategoryCtrl'
      })
      // BRANDS
      .state('brands', {
        url: '/marche',
        templateUrl: 'views/brands.html',
        controller: 'BrandsCtrl'
      })
     // SINGOLO BRAND
      .state('brand', {
        url: '/marche/:brand_slug',
        templateUrl: 'views/brand.html',
        controller: 'BrandCtrl'
      })
      .state('brandPaginated', {
        url: '/marche/:brand_slug/pagina/:page',
        templateUrl: 'views/brand.html',
        controller: 'BrandCtrl'
      })
     // UTENTE
      .state('user', {
        url: '/utenti/:user_slug',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      }) 
      .state('userPaginated', {
        url: '/utenti/:user_slug/pagina/:page',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      }) 
      // UTENTE SCOPERTI
      .state('userDiscovered', {
        url: '/utenti/:user_slug/scoperti',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .state('userDiscoveredPaginated', {
        url: '/utenti/:user_slug/scoperti/pagina/:page',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      // UTENTE COLLEZIONI
      .state('userCollections', {
        url: '/utenti/:user_slug/collezioni',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .state('userCollectionsPaginated', {
        url: '/utenti/:user_slug/collezioni/pagina/:page',
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
    // COLLEZIONI
      .state('collections', {
        url: '/collezioni',
        templateUrl: 'views/collections.html',
        controller: 'CollectionsCtrl'
      }) 
      .state('collectionsPaginated', {
        url: '/collezioni/pagina/:page',
        templateUrl: 'views/collections.html',
        controller: 'CollectionsCtrl'
      }) 
    // SINGOLA COLLEZIONE UTENTE
      .state('collection', {
        url: '/utenti/:user_slug/collezioni/:collection_slug',
        templateUrl: 'views/collection.html',
        controller: 'CollectionCtrl'
      })
      .state('collectionPaginated', {
        url: '/utenti/:user_slug/collezioni/:collection_slug/pagina/:page',
        templateUrl: 'views/collection.html',
        controller: 'CollectionCtrl'
      })
      .state('feed', {
        url: '/feed',
        templateUrl: 'views/feed.html',
        controller: 'FeedCtrl'
      })
      .state('forgotPassword', {
        url: '/recupero-password',
        templateUrl: 'views/recupero-password.html',
        controller: 'AuthCtrl'
      })
      .state('resetPassword', {
        url: '/modifica-password',
        templateUrl: 'views/cambia-password.html',
        controller: 'AuthCtrl'
      });
      
      // use the HTML5 History API
      $locationProvider.html5Mode(false).hashPrefix('!');
      //$locationProvider.html5Mode(true);

  }]).run(function ($rootScope, $state, amMoment, $analytics){

    $rootScope.page = {
      setTitle: function(title) {
          this.title = title + ' | TheAmazones!';
      },
      setMetaDesc: function(metaDesc) {
          this.metaDesc = metaDesc;
      },
      setOgTitle: function(ogTitle){
        this.ogTitle = ogTitle;
      },
      setOgDescription: function(ogDescription){
        this.ogDescription = ogDescription;
      },
      setOgType: function(ogType){
        this.ogType = ogType;
      },
      setOgUrl: function(ogUrl){
        this.ogUrl = ogUrl;
      },
      setOgImage: function(ogImage){
        this.ogImage = ogImage;
      }
    }

    $rootScope.$on('$stateChangeSuccess', function(){

      document.body.scrollTop = document.documentElement.scrollTop = 0;

      $rootScope.page.setTitle($state.current.name || 'TheAmazones');
      $rootScope.bodyClass = $state.current.name;

    });

    amMoment.changeLocale('it');

    $rootScope.$on('auth:logout-success', function() {
      console.info('goodbye');
      $state.reload();
      // track signin
      $analytics.eventTrack('Utente si è sloggato', { 
        category: 'Auth', label: 'Utente si è sloggato'
      });
    });

    
  });
