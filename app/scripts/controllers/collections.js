'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:CollectionsCtrl
 * @description
 * # CollectionsCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('CollectionsCtrl', 
  	['$scope', 'Collections', '$stateParams', '$sanitize',
  	function ($scope, Collections, $stateParams, $sanitize) {
    	
    	var collezioni;


    	/*
			##
			##
			##
			// CHEKS for SETTING PAGINATION
			##
			##
			##
	  	*/ 

		    if($stateParams.page){
	    		// SE HA PAGINAZIONE

		    	console.log('ha paginazione');
		    	collezioni = Collections.get({'page': $sanitize($stateParams.page)});

		  	} else {
		  		// ALTRIMENTI PRENDI CARDS CHE CI SONO

		  		console.log('non ha paginazione');
		  		collezioni = Collections.get();

		  	}

	  	/*
			##
			##
			##
			// END SETTING PAGINATION
			##
			##
			##
	  	*/


	  	// R E S O L V I N G
	  	// P R O M I S E


	    collezioni.$promise.then(function(data) {
	      $scope.Collezioni = data;
	      var currentPage = 1;
      	$scope.isPageLoaded = true;

      	 /* 

    		CHECK PAGINATION AND SET EVERYTHING

    		*/

    		// IF PAGINATION EXISTS AND I CAN STILL PAGINATE TROUGHT THE TOTAL PAGES

	    	if($stateParams.page && parseInt($stateParams.page) < $scope.Collezioni.pages.total_pages){

	    		currentPage = $stateParams.page;
	    		$scope.nextPage = parseInt($stateParams.page) + 1;
	    		$scope.prevPage = parseInt($stateParams.page) - 1;

	    	} else {
	    		$scope.prevPage = parseInt($stateParams.page) -1;
	    		$scope.nextPage = false;
	    	}

	    	// ELSE IF I DON'T HAVE ANY PAGINATION BUT I CAN ITERATE
	    	if($scope.Collezioni.pages.total_pages >= 2 && !$stateParams.page){
	    		$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = 2;
	    	} else if($scope.Collezioni.pages.total_pages == 1) {
					$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = false;
	    	}

			});

  }]);
