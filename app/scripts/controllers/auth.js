'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:AuthCtrl
 * @description
 * # AuthCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('AuthCtrl', 
  	['$scope', '$rootScope', '$state', '$auth', 
  	function ($scope, $rootScope, $state, $auth) {


  		$scope.handlePwdResetBtnClick = function() {

	      $auth.requestPasswordReset($scope.pwdResetForm)
	        .then(function(resp) { 
	          // handle success response
	      })
	        .catch(function(resp) { 
	          // handle error response
	      });
	    };


	     $scope.handleUpdatePasswordBtnClick = function() {
	      $auth.updatePassword($scope.updatePasswordForm)
	        .then(function(resp) { 
	          // handle success response
	        })
	        .catch(function(resp) { 
	          // handle error response
	        });
		    };


  		$rootScope.$on('auth:password-reset-request-success', function(user) {
	      $scope.resetPwdEmailSentSuccess = 'Ti abbiamo inviato le istruzioni per il recupero della password alla email da te inserita. Fanne buon uso!';
	    });

	    $rootScope.$on('auth:password-reset-request-error', function(user) {
	      $scope.resetPwdEmailSentError = 'Non ci risultano utenti con questa password. Sicuro di aver inserito una email corretta?';
	    });


	    $rootScope.$on('auth:password-change-success', function(user) {
	      $scope.editPwdEmailSentSuccess = 'Ti abbiamo inviato le istruzioni per il recupero della password alla email da te inserita. Fanne buon uso!';
	    });

	    $rootScope.$on('auth:password-change-error', function(error) {
	      $scope.editPwdEmailSentError =  'impossibile effettuare il reset della password.';
	    });

	    
   		
  }]);
