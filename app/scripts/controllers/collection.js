'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:CollectionCtrl
 * @description
 * # CollectionCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('CollectionCtrl', 
		['$scope', 'Collection', '$stateParams', '$sanitize', '$auth', '$state', 
		function ($scope, Collection, $stateParams, $sanitize, $auth, $state) {
    	
    	var	urlParameterRetrieved = {
			  		'user_slug' : $sanitize($stateParams.user_slug),
			  		'collection_slug' : $sanitize($stateParams.collection_slug)
			  	},  
    			collezione = {};

    	// GET REQUEST FOR FETCH THE PRODUCT
	    collezione = Collection.get({
	    	user_slug: urlParameterRetrieved.user_slug, 
	    	collection_slug: urlParameterRetrieved.collection_slug });

	    // EVALUATE THE PRODUCT AND RENDER IT TO THE BROWSER
	    collezione.$promise.then(function(data) {
	      $scope.Collection = data.collection;
	      $scope.isPageLoaded = true;
			});

			$scope.deleteCollection = function(){

				if($auth.user.signedIn){

					var cancellaCollezione = Collection.remove({
	    				user_slug: urlParameterRetrieved.user_slug, 
	    				collection_slug: urlParameterRetrieved.collection_slug 
	    			});

					cancellaCollezione.$promise.then(function(data){

						console.info('collezione cancellata');

						

						$state.go('userCollections', {user_slug: $auth.user.slug});


					}, function(error){

					});
	    		
	    	} else {
	    		console.info('Non sei loggato. E non potresti nemmeno averci cliccato qui');
	    	}
			};

}]);
