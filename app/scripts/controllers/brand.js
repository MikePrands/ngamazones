'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:BrandCtrl
 * @description
 * # BrandCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('BrandCtrl', 
  	['$scope', 'Brand', '$stateParams', '$sanitize', '$log',
  	function ($scope, Brand, $stateParams, $sanitize) {

  		// Retrieving the parameters from URL
	  	var urlParameterRetrieved = {
	  		'brand_slug' : $sanitize($stateParams.brand_slug)
	  	}, showBrand; 



	  	/*
			##
			##
			##
			// CHEKS for SETTING PAGINATION
			##
			##
			##
	  	*/ 

		    if($stateParams.page){
	    		// SE HA PAGINAZIONE

		    	console.log('ha paginazione');
		    	showBrand = Brand.get({brand_slug: urlParameterRetrieved.brand_slug, 'page': $sanitize($stateParams.page)}); 

		  	} else {
		  		// ALTRIMENTI PRENDI CARDS CHE CI SONO

		  		console.log('non ha paginazione');
		  		showBrand = Brand.get({brand_slug: urlParameterRetrieved.brand_slug});

		  	}

	  	/*
			##
			##
			##
			// END SETTING PAGINATION
			##
			##
			##
	  	*/



	  	// R E S O L V I N G
	  	// P R O M I S E
	    
	    showBrand.$promise.then(function(data) {

	    	var currentPage = 1;
      	$scope.isPageLoaded = true;
	      $scope.Brand = data;

	      /* 

    		CHECK PAGINATION AND SET EVERYTHING

    		*/

    		// IF PAGINATION EXISTS AND I CAN STILL PAGINATE TROUGHT THE TOTAL PAGES

	    	if($stateParams.page && parseInt($stateParams.page) < $scope.Brand.pages.total_pages){

	    		currentPage = $stateParams.page;
	    		$scope.nextPage = parseInt($stateParams.page) + 1;
	    		$scope.prevPage = parseInt($stateParams.page) - 1;

	    	} else {
	    		$scope.prevPage = parseInt($stateParams.page) -1;
	    		$scope.nextPage = false;
	    	}

	    	// ELSE IF I DON'T HAVE ANY PAGINATION BUT I CAN ITERATE
	    	if($scope.Brand.pages.total_pages >= 2 && !$stateParams.page){
	    		$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = 2;
	    	} else if($scope.Brand.pages.total_pages == 1) {
					$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = false;
	    	}

   		}, function(error){
   			console.error('errore: ' + error);
   		});



  }]);
