'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('SearchCtrl', 
  	['$scope', '$rootScope', 'Products', '$state', '$stateParams', '$sanitize',   
  	function ($scope, $rootScope, Products, $state, $stateParams, $sanitize) {

  		var search;

    	$scope.researchedString =  $sanitize($stateParams.q);
    		
    	// console.info('hai ricercato: ' + $stateParams.q);





    	if($stateParams.page){
    	// SE HA PAGINAZIONE

    	// console.log('ha paginazione');
	    	search = Products.get({'search': $sanitize($stateParams.q), 'page': $sanitize($stateParams.page)});
	  	} else {
	  		// ALTRIMENTI PRENDI PRODOTTI CHE CI SONO

	  		// console.log('non ha paginazione');
	  		search = Products.get({'search': $sanitize($stateParams.q)});
	  	}




    	 

    	search.$promise.then(function(data){

    		if(data.pages.products_count >= 1) {

    			var currentPage = 1;
    			$scope.Search = data;
    			$scope.isPageLoaded = true;
    			$scope.researchedElements = data.pages.products_count;

    			/* 

    					CHECK PAGINATION AND SET EVERYTHING

		    	*/

		    	// IF PAGINATION EXISTS AND I CAN STILL PAGINATE TROUGHT THE TOTAL PAGES
		    	if($stateParams.page && parseInt($stateParams.page) < data.pages.total_pages){

		    		currentPage = $stateParams.page;
		    		$scope.nextPage = parseInt($stateParams.page) + 1;
		    		$scope.prevPage = parseInt($stateParams.page) - 1;

		    	} else {
		    		$scope.prevPage = parseInt($stateParams.page) -1;
		    		$scope.nextPage = false;
		    	}

		    	// ELSE IF I DON'T HAVE ANY PAGINATION BUT I CAN ITERATE
		    	if(data.pages.total_pages >= 2 && !$stateParams.page){

		    		$scope.currentPage = 1;
		    		$scope.prevPage = false;
		    		$scope.nextPage = 2;
		    	} else if(data.pages.total_pages === 1) {

						$scope.currentPage = 1;
		    		$scope.prevPage = false;
		    		$scope.nextPage = false;
		    	}

    		} else {
    			
    			$scope.isPageLoaded = true;
    			$scope.researchedElements = 0;
    		}

    	}, function(error){
    			console.error('errore: ' + error);
    	});






  }]);
