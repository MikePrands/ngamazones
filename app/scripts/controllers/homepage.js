'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:HomepageCtrl
 * @description
 * # HomepageCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('HomepageCtrl', 
  	['$scope', 'Products', '$stateParams', '$sanitize', '$state','Feed', '$auth', 
  	function ($scope, Products, $stateParams, $sanitize, $state, Feed, $auth) {

      
      $scope.page.setTitle('I migliori prodotti di Amazon');
      $scope.page.setMetaDesc('sono la meta description');
      $scope.page.setOgTitle('I migliori prodotti di Amazon');
      $scope.page.setOgDescription('sono og description');
      $scope.page.setOgType('sono og website');
    

    var lista_prodotti;

		$scope.states = $stateParams.page;


    /*
      

          DISPLAY FEED


     */




    if($stateParams.page){
    	// SE HA PAGINAZIONE
    	// console.log('ha paginazione');
    	lista_prodotti = Products.get({'page': $sanitize($stateParams.page)});
  	} else {
  		// ALTRIMENTI PRENDI PRODOTTI CHE CI SONO
  		// console.log('non ha paginazione');
  		lista_prodotti = Products.get();
  	}



    // EVALUATE THE PRODUCT AND RENDER IT TO THE BROWSER
	  lista_prodotti.$promise.then(function(data) {
	  	var currentPage = 1;
      $scope.isPageLoaded = true;
	  			// prevPage = false,
	  			// nextPage = false;

    	$scope.products = data;

    	/* 

    		CHECK PAGINATION AND SET EVERYTHING

    	*/

    	// IF PAGINATION EXISTS AND I CAN STILL PAGINATE TROUGHT THE TOTAL PAGES
    	if($stateParams.page && parseInt($stateParams.page) < $scope.products.pages.total_pages){

    		currentPage = $stateParams.page;
    		$scope.nextPage = parseInt($stateParams.page) + 1;
    		$scope.prevPage = parseInt($stateParams.page) - 1;

    	} else {
    		$scope.prevPage = parseInt($stateParams.page) -1;
    		$scope.nextPage = false;
    	}

    	// ELSE IF I DON'T HAVE ANY PAGINATION BUT I CAN ITERATE
    	if($scope.products.pages.total_pages >= 2 && !$stateParams.page){
    		$scope.currentPage = 1;
    		$scope.prevPage = false;
    		$scope.nextPage = 2;
    	} else if($scope.products.pages.total_pages === 1) {
				$scope.currentPage = 1;
    		$scope.prevPage = false;
    		$scope.nextPage = false;
    	}
    	
    }); // END PROMISE

	  // PAGINATION
    $scope.goToNextPage = function(pageN){
			$state.go('homepagePaginated', {'page':pageN});
		};

		// PAGINATION
    $scope.goToPrevPage = function(pageN){
			$state.go('homepagePaginated', {'page':pageN});
		};

  }]);
