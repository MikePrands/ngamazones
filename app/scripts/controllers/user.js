'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('UserCtrl', 
  	['$scope', 'User', '$stateParams', '$sanitize', '$auth', 'FollowUser', 
  	function ($scope, User, $stateParams, $sanitize, $auth, FollowUser) {
    
    // SET VARIABLES
  		var utente = {},
			    // Retrieving the parameters from URL
			  	urlParameterRetrieved = { 'user_slug' : $sanitize($stateParams.user_slug) };			  	

	  	utente =  User.get({user_slug: urlParameterRetrieved.user_slug});

	  	// EVALUATE THE PRODUCT AND RENDER IT TO THE BROWSER
	    utente.$promise.then(function(data) {
	      $scope.Utente = data.user;
			});



			$scope.followUser = function(user){
				
				if($auth.user.signedIn){

					var following = FollowUser.follow({
						user_slug: user
					});

					following.$promise.then(function(resp){
						if(!$scope.Utente.followed_by_user){

							$scope.Utente.followers.unshift({
	    					'nickname' : $auth.user.nickname,
	    					'small_avatar' : $auth.user.small_avatar,
	    					'slug' : $auth.user.slug
	    				});

	    				$scope.Utente.followers_count ++;

	    				$scope.Utente.followed_by_user = true;

						} else {

							$scope.Utente.followers_count --;
							$scope.Utente.followers.shift();
	    				$scope.Utente.followed_by_user = false;
						}

					}, function(err){
							console.error(err);
					});

					
				} else {
					console.error('non sei loggato!');
				}

				
			};


  }]);
