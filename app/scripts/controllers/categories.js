'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:CategoriesCtrl
 * @description
 * # CategoriesCtrl
 * Controller of the theamazonesApp
 */
 
angular.module('theamazonesApp')
  .controller('CategoriesCtrl', 
  	['$scope', 'Categories', 
  	function ($scope, Categories) {

	    var categorie;

	    categorie = Categories.get();

	    categorie.$promise.then(function(data) {
	    	$scope.categories = data.categories;
	    });

  }]);
