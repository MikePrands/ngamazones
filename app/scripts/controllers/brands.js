'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:BrandsCtrl
 * @description
 * # BrandsCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('BrandsCtrl', 
  	['$scope', 'Brands', 
  	function ($scope, Brands) {

  		var manufacturers = Brands.get();

  		manufacturers.$promise.then(function(data) {
  			$scope.Brands = data.manufacturers;
  		});

  		
    
  }]);
