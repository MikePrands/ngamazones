'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('HeaderCtrl', 
    ['$scope', '$auth', '$state', 
    function ($scope, $auth, $state) {

    // $scope.$state = $state;

    // $scope.isLoggedIn = false;

    $scope.handleSignOutBtnClick = function() {
      $auth.signOut()
        .then(function(resp) { 
          // handle success response
          console.info('m\'hai sloggato: ' + resp);
          $scope.authLoading = false;
        })
        .catch(function(resp) { 
          // handle error response
          console.error('errore: ' + resp);
        });
        $scope.authLoading = false;
        $state.reload();
    };
    
  }]);
