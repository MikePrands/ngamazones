'use strict';

	angular
		.module('theamazonesApp')
	  .controller('SingleProductCtrl', 
	  	['$scope', 'Product', '$stateParams', '$sanitize', '$auth', 'Comment', '$analytics', 
	  	function ($scope, Product, $stateParams, $sanitize, $auth, Comment, $analytics ) {

			var urlParameterRetrieved = {'product_slug' : $sanitize($stateParams.product_slug)},
					prodotto, 
			    commento,
			    act_comment;

	    // GET REQUEST FOR FETCH THE PRODUCT
	    prodotto = Product.getProduct({product_slug: urlParameterRetrieved.product_slug});

	    // EVALUATE THE PRODUCT AND RENDER IT TO THE BROWSER
	    prodotto.$promise.then(function(data) {
	      $scope.product = data.product;
	      $scope.isPageLoaded = true;
			}, 
			function(error){
				console.error('si è verificato un errore: ' + error);
			});

	    // COMMENT ACTION
	    $scope.submitComment = function(theComment){

	    	commento = {
	    		'contenuto': theComment.commentText, 
	    		'user_slug': $auth.user.slug,
	    		'product_slug': urlParameterRetrieved.product_slug
	    	};

	    	console.info(commento);

	   		act_comment = Comment.comment({
	   			product_slug: commento.product_slug,
	   			text: commento.contenuto, 
	   			user_slug: commento.user_slug
	   		});

	   		act_comment.$promise.then(function(resp){

	    		console.info('messaggio inviato correttamente: ' + resp);

	    		// track comment
		      $analytics.eventTrack('Utente ha commentato un prodotto', { 
		        category: 'Comment', label: 'Utente ha commentato un prodotto'
		      });

	    			$scope.product.comments_count ++;

	    			$scope.product.comments.push({
			    		'created_at' : new Date(), 
			    		'text' : commento.contenuto,
			    		'nickname' : $auth.user.nickname,
			    		'small_avatar' : $auth.user.small_avatar
			    	});

	    		// Ripulisco il campo form
		    	$scope.commentForm = {};

	    	}, function(error){
	    		console.error('errore: ' + error);
	    		// $scope.commentForm.commentText = 'Errore: ' + error;
	    	});	 

	    };

}]); // END 
