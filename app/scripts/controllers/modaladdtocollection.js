'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:ModaladdtocollectionCtrl
 * @description
 * # ModaladdtocollectionCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('ModaladdtocollectionCtrl',
  	['$scope', 'Product', 'Collections', '$sanitize', '$auth',
  	function ($scope, Product, Collections, $sanitize) {


    	var vm = $scope,
    			collections = Collections.query();

    	collections.$promise.then(function(data){

    		vm.collections = data;

    		// Visualizzo -onclick la lista delle collezioni dell'utente
				vm.showUserCollections = function(){
			 		console.log('apro lista collezione');
			 	};

			 	vm.addToCollection = function(product_id, collection_id){
			 		console.log('aggiungendo il prodotto ' + product_id + ' alla collezione ' + collection_id + ' !!!');
			 	};

			 	// Salvo la nuova collezione dell'utente
			 	vm.openNewCollectionModal = function(){
			 		console.log('apro il modale per creare una nuova collezione');
			 	};

			 	// Salvo la nuova collezione dell'utente
			 	vm.saveNewCollection = function(){
			 		console.log('sto salvando la nuova collezione: ' + $sanitize(vm.newCollectionName) );
			 		
			 		// vm.collections.unshift({
			 		// 	'name': $sanitize(vm.newCollectionName)
			 		// });

			 		// Clear input fields after push
        	vm.newCollectionName = '';
			 	};

    	});
	}]);
