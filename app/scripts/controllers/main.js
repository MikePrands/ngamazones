'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:MainCtrl
 * @description
		
		Gestisce l'intero viewport



 * # MainCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('MainCtrl', 
  	['$scope', '$state', 'Product', 'UserCollections', '$analytics', 
  		function ($scope, $state, Product, UserCollections, $analytics) {
            
    	$scope.$state = $state;

			/*


			
    				OPEN MODALS 
    				FUNCTION 



			*/
        	$scope.openModal = function(modalType, productSlug){
        		switch(modalType){
        			case 'signin':
        				$scope.isModalSignIn = true;
                        // track modal opened
                        $analytics.eventTrack('Aperto modale sign-in', { 
                          category: 'Open Modal', label: 'aperto modale signin'
                        }); 
        			break;

        			case 'signup':
        				$scope.isModalSignUp = true;
                        // track modal opened
                        $analytics.eventTrack('Aperto modale sign-up', { 
                          category: 'Open Modal', label: 'aperto modale sign-up'
                        });
        			break;

        			case 'editprofile':
        				$scope.isModalEditProfile = true;
                        // track modal opened
                        $analytics.eventTrack('Aperto modale edit-profile', { 
                          category: 'Open Modal', label: 'aperto modale edit-profile'
                        });
        			break;

        			case 'addtocollections':
        				$scope.isModalCollections = true;
                        // $state.reload();
                        // track modal opened
                        $analytics.eventTrack('Aperto modale add-to-collections', { 
                          category: 'Open Modal', label: 'aperto modale add-to-collections'
                        });
        			break;

        			case 'addproduct':
        				$scope.isModalAddProduct = true;
                        // track modal opened
                        $analytics.eventTrack('Aperto modale add-product', { 
                          category: 'Open Modal', label: 'aperto modale add-product'
                        });
        			break;
        		}

                if(productSlug){
                    console.info('sembra un\'aggiunta ad una collezione!');

                    // $state.reload();

                    // GET REQUEST FOR FETCH THE PRODUCT
                    var retrieveProduct = Product.getProduct({product_slug: productSlug});

                    retrieveProduct.$promise.then(function(data){
                        $scope.inCollectionProduct = data.product;

                        // $scope.isModalCollections = true;
                    }, function(error){
                        console.error(error);
                    });

                    /*
                    *
                    *
                    *
                            #####   Process collection Service
                    *
                    *
                    *
                    */

                    var collezioniUtente = UserCollections.get({
                        user_slug: $scope.user.slug,
                        product_slug: productSlug
                    });                    

                    /*
                    *
                    *
                    *
                            #####   GET ALL COLLECTIONS FROM CERTAIN USER
                    *
                    *
                    *
                    */

                    collezioniUtente.$promise.then(function(data){
                        $scope.CollezioniUtente = data.collections;
                    });
                }
        	};

    	   /*



    				CLOSE MODALS 
    				FUNCTION 

			*/

			$scope.closeModal = function(modalType){

        		switch(modalType){
        			case 'signin':
        				$scope.isModalSignIn = false;
                        angular.element(document.querySelector('body')).removeClass('modal-open'); 
                        // $state.reload();
        			break;

        			case 'signup':
        				$scope.isModalSignUp = false;
                        angular.element(document.querySelector('body')).removeClass('modal-open'); 
                        // $state.reload();
        			break;

        			case 'editprofile':
        				$scope.isModalEditProfile = false;
                        angular.element(document.querySelector('body')).removeClass('modal-open'); 
                        // $state.reload();
        			break;

        			case 'addtocollections':
        				$scope.isModalCollections = false;
                        angular.element(document.querySelector('body')).removeClass('modal-open'); 
                        // $state.reload();
        			break;

        			case 'addproduct':
        				$scope.isModalAddProduct = false;
                        angular.element(document.querySelector('body')).removeClass('modal-open'); 
                        // $state.reload();
        			break;
        		}
    	   };

           

        $scope.submitSearch = function(theString){
            if(theString.search.length >= 3){
                $state.go('search', {q: theString.search});
                console.info('hai ricercato: ' + theString.search)
            } else {
                $state.go('homepage');
            }
        };
}]);
