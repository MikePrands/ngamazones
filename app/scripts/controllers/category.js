'use strict';

/**
 * @ngdoc function
 * @name theamazonesApp.controller:CategoryCtrl
 * @description
 * # CategoryCtrl
 * Controller of the theamazonesApp
 */
angular.module('theamazonesApp')
  .controller('CategoryCtrl', 
  	['$scope', 'Category', '$stateParams', '$sanitize',
  	function ($scope, Category, $stateParams, $sanitize) {

  		// Retrieving the parameters from URL
	  	var urlParameterRetrieved = {
		  			'category_slug' : $sanitize($stateParams.category_slug)
		  		},
	  			showCategory;

	    // $scope.states = $stateParams.page;



	    
	    /*
			##
			##
			##
			// CHEKS for SETTING PAGINATION
			##
			##
			##
	  	*/ 

		    if($stateParams.page){
	    		// SE HA PAGINAZIONE

		    	console.log('ha paginazione');
		    	showCategory = Category.get({category_slug: urlParameterRetrieved.category_slug, 'page': $sanitize($stateParams.page)});

		  	} else {
		  		// ALTRIMENTI PRENDI CARDS CHE CI SONO

		  		console.log('non ha paginazione');
		  		showCategory = Category.get({category_slug: urlParameterRetrieved.category_slug});

		  	}

	  	/*
			##
			##
			##
			// END SETTING PAGINATION
			##
			##
			##
	  	*/





	  	// R E S O L V I N G
	  	// P R O M I S E


	  	showCategory.$promise.then(function(data) {

	  		var currentPage = 1;
      	$scope.isPageLoaded = true;
	      $scope.category = data;

	      // console.log(data);

	      /* 

    		CHECK PAGINATION AND SET EVERYTHING

    		*/

    		// IF PAGINATION EXISTS AND I CAN STILL PAGINATE TROUGHT THE TOTAL PAGES

	    	if($stateParams.page && parseInt($stateParams.page) < $scope.category.pages.total_pages){

	    		currentPage = $stateParams.page;
	    		$scope.nextPage = parseInt($stateParams.page) + 1;
	    		$scope.prevPage = parseInt($stateParams.page) - 1;

	    	} else {
	    		$scope.prevPage = parseInt($stateParams.page) -1;
	    		$scope.nextPage = false;
	    	}

	    	// ELSE IF I DON'T HAVE ANY PAGINATION BUT I CAN ITERATE
	    	if($scope.category.pages.total_pages >= 2 && !$stateParams.page){
	    		$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = 2;
	    	} else if($scope.category.pages.total_pages == 1) {
					$scope.currentPage = 1;
	    		$scope.prevPage = false;
	    		$scope.nextPage = false;
	    	}


   		}, function(error){
   			console.error('errore: ' + error);
   		});






	  	

	    // showCategory = Category.get({category_slug: urlParameterRetrieved.category_slug});
	    
}]);
