'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:headerUserSignin
 * @description
 * # headerUserSignin
 */
angular.module('theamazonesApp')
  .directive('headerUserSignin', function () {
    return {
      templateUrl: 'views/partials/header-user-signin.html',
      restrict: 'A'    
    };
  });
