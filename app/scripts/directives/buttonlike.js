'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:buttonLike
 * @description
 * # buttonLike
 */
angular.module('theamazonesApp')
  .directive('buttonLike', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the buttonLike directive');
      }
    };
  });
