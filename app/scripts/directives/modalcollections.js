'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:modalCollections
 * @description
 * # modalCollections
 */
angular.module('theamazonesApp')
  .directive('modalCollections', function () {
    return {
      templateUrl: 'views/partials/modal-addtocollection.html',
	      restrict: 'A',
	      link: function postLink() {
	        // element.text('this is the addtoCollection directive');
	        // element.bind('click', function(){
	        // 	$scope.$apply(attrs.addtoCollection);
	        // });
	      },
	      controller: ['$scope', '$auth', 'UserCollections', 'AddUserCollection', 'AddProductToCollection', '$analytics', 
	      function($scope, $auth, UserCollections, AddUserCollection, AddProductToCollection, $analytics){
	      	

	      	$scope.openModalNewCollection = false;


	      	$scope.newCollectionForm = function(){
	      		$scope.openModalNewCollection = true;
	      	};

	      	$scope.closeNewCollectionForm = function(){
	      		$scope.openModalNewCollection = false;
	      	};

					/*
					*
					*
					*
					#####	IF USER IS LOGGED
					*
					*
					*
					*/



	      	if($auth.user.signedIn){
	      		/*
	      		*
	      		*
	      		*
						#####	CREATE NEW COLLECTION
	      		*
	      		*
	      		*
	      		*/
	      		$scope.createCollection = function(collectionName){
	      			console.info(collectionName);

	      			var createNewCollection = AddUserCollection.addCollection({
	      				user_slug: $auth.user.slug,
	      				name: collectionName
	      			});

	      			createNewCollection.$promise.then(function(resp){
	      				$scope.CollezioniUtente.unshift({
	      					'name' : resp.collection.name,
	      					'slug' : resp.collection.slug,
	      					'products' : []
	      				});

	      				// track create collection
					      $analytics.eventTrack('Utente ha creato una collezione', { 
					        category: 'Collections', label: 'Utente ha creato una collezione'
					      });

	      			}, function(err){
	      					console.error(err);
	      			});
	      		};


	      		/*
	      		*
	      		*
	      		*
						#####	ADD PRODUCT TO COLLECTION
	      		*
	      		*
	      		*
	      		*/

	      		$scope.toggleInCollection = function(indice, product_slug, collection_slug, productImageUrl){

	      			// console.info('indice: ' + indice);
	      			// console.info('product_slug: ' + product_slug + ' collection_slug: ' + collection_slug);





	      			var addProductToCollection = AddProductToCollection.addProduct({

	      				user_slug: $auth.user.slug,
	      				collection_slug: collection_slug,
	      				product_id: product_slug

	      			});
	      			

	      			addProductToCollection.$promise.then(function(resp){

	      				if($scope.CollezioniUtente[indice].product_in_collection){

	      					$scope.CollezioniUtente[indice].products.shift();
	      					$scope.CollezioniUtente[indice].product_in_collection = false;
	      					$scope.CollezioniUtente[indice].products_count --;

	      					// track add to collection
						      $analytics.eventTrack('Utente ha inserito un prodotto in una collezione', { 
						        category: 'Collections', label: 'Utente ha rimosso un prodotto da una collezione'
						      });
	      				} else {

	      					$scope.CollezioniUtente[indice].products.unshift({
		      					'image_url': productImageUrl
		      				});

	      					$scope.CollezioniUtente[indice].product_in_collection = true;

	      					if(isNaN($scope.CollezioniUtente[indice].products_count)){
	      						$scope.CollezioniUtente[indice].products_count = 0;
	      						$scope.CollezioniUtente[indice].products_count ++;
	      					} else {
	      						$scope.CollezioniUtente[indice].products_count ++;
	      					}
	      					
	      					// track add to collection
						      $analytics.eventTrack('Utente ha inserito un prodotto in una collezione', { 
						        category: 'Collections', label: 'Utente ha inserito un prodotto in una collezione'
						      });

	      					
	      				}

	      			}, function(err){

	      					console.info('Errore: nell\' aggiunta del prodotto');
	      			});


	      		};


	      	} else {

						/*
						*
						*
						*
						#####	IF USER IS NOT LOGGED
						*
						*
						*
						*/
	      		// Error
	      		// console.error('Non sei loggato figlio di puttana.');
	      	}
	      }]
    };
  });
