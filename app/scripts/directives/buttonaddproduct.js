'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:buttonAddproduct
 * @description
 * # buttonAddproduct
 */
angular.module('theamazonesApp')
  .directive('buttonAddproduct', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the buttonAddproduct directive');
      }
    };
  });
