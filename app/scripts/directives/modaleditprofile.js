'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:editProfile
 * @description
 * # editProfile
 */
angular.module('theamazonesApp')
  .directive('modalEditProfile', function () {
    return {
      templateUrl: 'views/partials/modal-editprofile.html',
      restrict: 'A',
      controller: 
      ['$scope', '$auth', '$rootScope', '$upload', 'TheEnvironment', 
      function($scope, $auth, $rootScope, $upload, TheEnvironment){

      	$scope.editingProfile = false;
        $scope.updatingProfile = false;
        $scope.profileUpdated = false;

      	$rootScope.$on('auth:validation-success', function(ev, user) {
          // console.log($rootScope.user);
	      	$scope.updateAccountForm = {
		  			name: $auth.user.name,
		  			email: $auth.user.email,
		  			bio: $auth.user.bio,
            link: $auth.user.link,
            image: $auth.user.image
		  		};
	      });


        $scope.updateProfile = function(){
          $scope.updatingProfile = true;
        }

	  		// HANDLE CHANGE PASSWORD

	  		$scope.handleUpdateAccountBtnClick = function() {
	      	$auth.updateAccount($scope.updateAccountForm)
	        .then(function(resp) { 
	          // handle success response
	          console.info(resp);
	        })
	        .catch(function(resp) { 
	          // handle error response
	          console.error(resp);
	        });
	    	};

	    	$rootScope.$on('auth:account-update-success', function(ev, user) {
	         // console.log($rootScope.user);
	         // console.info(user);
           $scope.profileUpdated = true;
            $scope.updatingProfile = false;

	      });

		    $scope.upload = function (files) {
	        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              $upload.upload({
                  url: TheEnvironment.apiUrl + '/auth/', // 'http://localhost:3000/auth/', 
                  method: 'PUT',
                  headers: $auth.retrieveData('auth_headers'),
                  fileFormDataName: 'image', 
                  file: file
              }).progress(function (evt) {
              		$scope.editingProfile = true;
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ');
              }).success(function (data, status, headers, config) {
              		$scope.editingProfile = false;
              		$scope.user.large_avatar = data.data.large_avatar;
                  console.log('file ' + config.file.name + ' uploaded. Response: ' + data);
              }).error(function(data) {
                  console.log(data.error);
                  $scope.editingProfile = false;
              });
            }
	        }
		    };


      }]
    };
  });
