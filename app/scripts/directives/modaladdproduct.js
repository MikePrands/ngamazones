'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:modalAddProduct
 * @description
 * # modalAddProduct
 */
angular.module('theamazonesApp')
  .directive('modalAddProduct', function () {
    return {
      templateUrl: 'views/partials/modal-addproduct.html',
      restrict: 'A',
      controller: function ($scope, $auth, AddProduct, $state, $sanitize, $analytics){


          // $scope.$copy(function(){
            $scope.isLoading = false;
            $scope.productAddedSuccess = false;
            $scope.productAddedHasError = false;
            $scope.producturl = '';
          // });

        $scope.resetModalProduct = function(){
          $scope.isLoading = false;
          $scope.productAddedSuccess = false;
          $scope.productAddedHasError = false;
          $scope.producturl = '';
        }
        

      	$scope.addProduct = function (product_url){
      		$scope.isLoading = true;
          $scope.productAddedHasError = false;

          var product_url = $sanitize(pastedLink.originalEvent.clipboardData.getData('text/plain'));

          AddProduct.save({url: product_url}).$promise.then(function(resp){
            // console.info(resp);

            $scope.isLoading = false;
            
            // $state.go('product', {product_slug: resp.slug});
            // angular.element(document.querySelector('body')).removeClass('modal-open');

            $scope.productAddedSuccess = true;

            $scope.previewAddedProduct = resp;

            // $state.reload();
          }, function(error){
            // console.error(error);
            $scope.productAddedHasError = true;
            $scope.productError = error.message;
          });
      	};

        $scope.addProductPasted = function (pastedLink){

          $scope.isLoading = true;
          $scope.productAddedHasError = false;

          var product_url = $sanitize(pastedLink.originalEvent.clipboardData.getData('text/plain'));

          AddProduct.save({url: product_url}).$promise.then(function(resp){
            
            // console.info(resp);

            $scope.isLoading = false;
            
            // $state.go('product', {product_slug: resp.slug});
            // angular.element(document.querySelector('body')).removeClass('modal-open');

            // track modal opened
            $analytics.eventTrack('Add product', { 
              category: 'Product added', label: ''
            });

            $scope.productAddedSuccess = true;

            $scope.previewAddedProduct = resp;

            // $state.reload();
          }, function(error){
            // console.error(error);
            $scope.isLoading = false;
            $scope.productAddedHasError = true;
            $scope.productError = 'Il link che hai inserito non è valido. Inserisci un link valido';
          });
        };
      }
    };
  });
