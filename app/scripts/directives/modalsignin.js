'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:modalSignin
 * @description
 * # modalSignin
 */
angular.module('theamazonesApp')
  .directive('modalSignIn', function () {
    return {
      templateUrl: 'views/partials/modal-signin.html',
      restrict: 'A',
      controller: 
      ['$scope', '$rootScope', '$auth', '$state', '$analytics', 
      function($scope, $rootScope, $auth, $state, $analytics){

      	$scope.authLoading = false;

      	$scope.handleLoginBtnClick = function() {

		      $auth.submitLogin($scope.loginForm)
	        .then(function(resp) { 
	        	$scope.authLoading = true;
	          // handle success response
	          angular.element(document.querySelector('body')).removeClass('modal-open'); 
	          $state.reload();
	          console.info('sono loggato: ' + resp);
	          $scope.isLoggedIn = true;
	          $scope.authLoading = false;
	        })
	        .catch(function(resp) { 
	          // handle error response
	          console.error('errore: ' + resp);
	          $scope.authLoading = false;
	        });

	        $auth.authenticate('facebook')
	        .then(function(resp) { 
	          // handle success
	          angular.element(document.querySelector('body')).removeClass('modal-open'); 
	          $state.reload();
	          console.info('sono loggato: ' + resp);
	          $scope.isLoggedIn = true;
	          $scope.authLoading = false;

	        })
	        .catch(function(resp) { 
	          // handle errors
	          console.error('errore: ' + resp);
	          $scope.authLoading = false;
	        });

	        $auth.authenticate('twitter')
	        .then(function(resp) { 
	          // handle success
	          angular.element(document.querySelector('body')).removeClass('modal-open'); 
	          $state.reload();
	          console.info('sono loggato: ' + resp);
	          $scope.isLoggedIn = true;
	          $scope.authLoading = false;
	        })
	        .catch(function(resp) { 
	          // handle errors
	          console.error('errore: ' + resp);
	          $scope.authLoading = false;
	        });
		    };

		    $scope.loadAuth = function(){
		    	$scope.authLoading = true;
		    };


		    $rootScope.$on('auth:login-success', function(user) {
		      // console.info('benvenuto: ', user.nickname);
		      
		      angular.element(document.querySelector('body')).removeClass('modal-open'); 
		      $state.reload();

		      // track signin
		      $analytics.eventTrack('Utente si è loggato', { 
		        category: 'Auth', label: 'Utente si è loggato'
		      });

		    });

		    $rootScope.$on('auth:login-error', function(user) {
		      console.error('User o password errata. Riprova: ', user.nickname);
		      $scope.loginError = 'Email o password sbagliata. Riprova.'

		      // $state.reload();
		    });

      }]
    };
  });
