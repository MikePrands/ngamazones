'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:productSinglePreview
 * @description
 * # productSinglePreview
 */
angular.module('theamazonesApp')
  .directive('productSinglePreview', function () {
    return {
      templateUrl: 'views/partials/product-single-preview-v2.html',
      restrict: 'A',
      // link: function postLink(scope, element, attrs) {
      //   element.text('this is the productSinglePreview directive');
      // }
    };
  });
