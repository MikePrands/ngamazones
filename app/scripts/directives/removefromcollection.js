'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:RemoveFromCollection
 * @description
 * # RemoveFromCollection
 */
angular.module('theamazonesApp')
  .directive('removeFromCollection', function () {
    return {
      templateUrl: 'views/partials/remove-product-from-collection.html',
      restrict: 'A',
      controller: function($scope, $auth, $state, RemoveItemCollection ){

      	$scope.removeProductFromCollection = function(productId, collectionSlug, index){

      		var removingProduct = RemoveItemCollection.remove({
      			user_slug: $auth.user.slug,
      			collection_slug: collectionSlug,
      			item_id: productId
      		});

      		removingProduct.$promise.then(function(resp){

      			// console.info('prodotto rimosso' + resp);
            console.info('indice: ' + index);
      			$scope.Collection.products.splice(index, 1);
            // $state.reload();
      		}, function(err){

      			console.error(err);
      		});

      	};

      }
    };
  });
