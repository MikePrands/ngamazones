'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:modalSignUp
 * @description
 * # modalSignUp
 */
angular.module('theamazonesApp')
  .directive('modalSignUp', function () {
    return {
      templateUrl: 'views/partials/modal-signup.html',
      restrict: 'A',
      controller: 
      ['$scope', '$auth', '$state', 
      function($scope, $auth, $state){


      	$scope.handleLoginBtnClick = function() {

	        $auth.authenticate('facebook')
	        .then(function(resp) { 
	          // handle success
	          angular.element(document.querySelector('body')).removeClass('modal-open'); 
	          $state.reload();
	          console.info('sono loggato: ' + resp);
	          $scope.isLoggedIn = true;

	        })
	        .catch(function(resp) { 
	          // handle errors
	          console.error('errore: ' + resp);
	        });

	        $auth.authenticate('twitter')
	        .then(function(resp) { 
	          // handle success
	          angular.element(document.querySelector('body')).removeClass('modal-open'); 
	          $state.reload();
	          console.info('sono loggato: ' + resp);
	          $scope.isLoggedIn = true;
	        })
	        .catch(function(resp) { 
	          // handle errors
	          console.error('errore: ' + resp);
	        });
		    };
    	  
    	  $scope.handleRegBtnClick = function() {
		      $auth.submitRegistration($scope.registrationForm)
			      .then(function(resp) { 
			        // handle success response
			        console.info('Registrato' + resp);
			      })
			      .catch(function(error) { 
			        // handle error response
			        console.error('errore: ' + error);
			      });
		    	};
    		}]
    };
  });
