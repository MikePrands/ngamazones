'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:productListLikes
 * @description
 * # productListLikes
 */
angular.module('theamazonesApp')
  .directive('productListLikes', function () {
    return {
      templateUrl: 'views/partials/product-list-likes.html',
      restrict: 'A',
      // link: function postLink(scope, element, attrs) {
      //   element.text('this is the productListLikes directive');
      // }
      controller: function(){
      	
      }
    };
  });
