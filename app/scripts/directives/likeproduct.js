'use strict';

/**
 * @ngdoc directive
 * @name theamazonesApp.directive:likeProduct
 * @description
 * # likeProduct
 */
angular.module('theamazonesApp')
  .directive('likeProduct', function () {
    return {
      templateUrl: 'views/partials/like-product.html',
      restrict: 'A',
      controller: ['$scope', '$auth', 'Product', 'Vote', '$analytics', 
      function ($scope, $auth, Product, Vote, $analytics){

      	// if($scope.is_liked){
      	// 	console.log('prodotto già piaciuto');
      	// }


      	/*

						FN likeProduct( slug prodotto )
						GESTIONE DEL CLICK SULL'ICONA LIKE

      	*/
			 	$scope.likeProduct = function(product_slug){

			 		/*

			 			Se l'utente è loggato potrà aggiungere o togliere
			 			il like dal prodotto. 

			 			Altrimenti invito per registrarsi / accedere

			 		*/
			 		if($auth.user.signedIn){

			 			/* 

			 				L'utente NON HA ancora fatto like
			 				può quindi aggiungere un like.

			 				### Come funziona?

			 				SE dal backend product.liked_by_user = true

							rendo l'icona LIKE -> attiva


							SE dal backend product.liked_by_user = false
							rendo l'icona LIKE -> disattiva


			 			*/
			 			var iconLike = angular.element('#like-product-btn');
			 			var liked = angular.element('#like-product-btn').hasClass('not-liked');

			 			var vote = Vote.vote({product_slug: $scope.product.slug});

			 			vote.$promise.then(function(){

			 				if($scope.product.liked_by_user == false){

			 					//  IF USER LIKE THE PRODUCT
			 					$scope.product.likes.unshift({
				 					'nickname' : $auth.user.nickname,
				 					'small_avatar': $auth.user.small_avatar 
				 				});

				 				if(!$scope.product.discoverer_nickname){
				 					$scope.product.discoverer_nickname = $auth.user.nickname;
				 					$scope.product.discoverer_name = $auth.user.name;
				 				}

				 				$scope.product.liked_by_user = true;
				 				$scope.product.likes_count ++;

				 				// track modal opened
                $analytics.eventTrack('Aggiunto Like a prodotto', { 
                  category: 'Like toggle', label: 'è stato aggiunto un like'
                });

			 				} else {

			 					//  IF USER DISLIKE THE PRODUCT

			 					$scope.product.likes.shift();

				 				$scope.product.liked_by_user = false;
				 				$scope.product.likes_count --;


				 				if($scope.product.discoverer_nickname == $auth.user.nickname){
				 					$scope.product.discoverer_nickname = null;
				 					$scope.product.discoverer_name = null;
				 				}

				 				// track modal opened
	              $analytics.eventTrack('Rimosso Like a prodotto', { 
	                category: 'Like toggle', label: 'è stato rimosso un like'
	              });

			 				}
			 			});

			 			//  END VOTE TOGGLE

		 				// track modal opened
            $analytics.eventTrack('Aggiunto Like a prodotto', { 
              category: 'Like toggle', label: 'è stato aggiunto un like'
            });


			 			// var liked = $scope.liked;

			 			// if( liked ){

			 			// 	iconLike.removeClass('not-liked').addClass('liked');
			 			// 	console.info('A utente ' + $auth.user.nickname + ' piace il prodotto ' + product_slug);
			 				
				 		// 		$scope.product.likes.unshift({
				 		// 			'nickname' : $auth.user.nickname,
				 		// 			'small_avatar': $auth.user.small_avatar 
				 		// 		});

				 		// 		$scope.product.liked_by_user = true;
				 		// 		$scope.product.likes_count ++;

				 		// 		Vote.vote({product_slug: $scope.product.slug});

				 		// 		// track modal opened
       //          $analytics.eventTrack('Aggiunto Like a prodotto', { 
       //            category: 'Like toggle', label: 'è stato aggiunto un like'
       //          });

			 			// } 

			 			// else if( !liked ){

			 			// 	iconLike.removeClass('liked').addClass('not-liked');
			 			// 	console.info('A utente ' + $auth.user.nickname + ' NON piace più il prodotto ' + product_slug);

			 			// 	$scope.product.likes.shift();

			 			// 	Vote.vote({product_slug: $scope.product.slug});

			 			// 	$scope.product.liked_by_user = false;
			 			// 	$scope.product.likes_count --;

			 			// 	// track modal opened
       //        $analytics.eventTrack('Rimosso Like a prodotto', { 
       //          category: 'Like toggle', label: 'è stato rimosso un like'
       //        });

			 			// }
			 			/* 

			 				L'utente HA GIA fatto like: 
			 				può quindi togliere il like

			 			*/
			 		} 

			 		/*

			 			L'utente NON è loggato quindi NON potrà aggiungere o togliere
			 			il like dal prodotto. 
			 			
			 			Faccio partire il messaggio / invito per registrarsi o accedere

			 		*/

			 		else {
			 			// toaster.pop('error', 'Attenzione', 'Per effettuare il like devi aver effettuato l\'accesso');
			 		}

			 	};

      }]
    };
  });
