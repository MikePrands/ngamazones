'use strict';

describe("Controller: main controller", function(){
	// load the service's module
  beforeEach(module('theamazonesApp'));

  // load the controller's module
  beforeEach(module('theamazonesApp'));

  var MainCtrl,
    scope, SEO;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FakecontrollerCtrl = $controller('MainCtrl', {
      $scope.SEO = SEO;
    });
  }));

  it('should attach SEO to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
