'use strict';

describe('Controller: BrandsCtrl', function () {

  // load the controller's module
  beforeEach(module('theamazonesApp'));

  var BrandsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BrandsCtrl = $controller('BrandsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
