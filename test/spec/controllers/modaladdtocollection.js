'use strict';

describe('Controller: ModaladdtocollectionCtrl', function () {

  // load the controller's module
  beforeEach(module('theamazonesApp'));

  var ModaladdtocollectionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaladdtocollectionCtrl = $controller('ModaladdtocollectionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
