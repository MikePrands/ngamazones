'use strict';

describe('Service: ProdottoSingolo', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var ProdottoSingolo;
  beforeEach(inject(function (_ProdottoSingolo_) {
    ProdottoSingolo = _ProdottoSingolo_;
  }));

  it('should do something', function () {
    expect(!!ProdottoSingolo).toBe(true);
  });

});
