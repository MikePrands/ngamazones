'use strict';

describe('Service: AddUserCollection', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var AddUserCollection;
  beforeEach(inject(function (_AddUserCollection_) {
    AddUserCollection = _AddUserCollection_;
  }));

  it('should do something', function () {
    expect(!!AddUserCollection).toBe(true);
  });

});
