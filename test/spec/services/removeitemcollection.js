'use strict';

describe('Service: RemoveItemCollection', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var RemoveItemCollection;
  beforeEach(inject(function (_RemoveItemCollection_) {
    RemoveItemCollection = _RemoveItemCollection_;
  }));

  it('should do something', function () {
    expect(!!RemoveItemCollection).toBe(true);
  });

});
