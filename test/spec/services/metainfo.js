'use strict';

describe('Service: MetaInfo', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var MetaInfo;
  beforeEach(inject(function (_MetaInfo_) {
    MetaInfo = _MetaInfo_;
  }));

  it('should do something', function () {
    expect(!!MetaInfo).toBe(true);
  });

});
