'use strict';

describe('Service: ListaProdotti', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var ListaProdotti;
  beforeEach(inject(function (_ListaProdotti_) {
    ListaProdotti = _ListaProdotti_;
  }));

  it('should do something', function () {
    expect(!!ListaProdotti).toBe(true);
  });

});
