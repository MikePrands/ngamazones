'use strict';

describe('Service: AddProductToCollection', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var AddProductToCollection;
  beforeEach(inject(function (_AddProductToCollection_) {
    AddProductToCollection = _AddProductToCollection_;
  }));

  it('should do something', function () {
    expect(!!AddProductToCollection).toBe(true);
  });

});
