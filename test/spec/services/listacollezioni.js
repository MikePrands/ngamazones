'use strict';

describe('Service: ListaCollezioni', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var ListaCollezioni;
  beforeEach(inject(function (_ListaCollezioni_) {
    ListaCollezioni = _ListaCollezioni_;
  }));

  it('should do something', function () {
    expect(!!ListaCollezioni).toBe(true);
  });

});
