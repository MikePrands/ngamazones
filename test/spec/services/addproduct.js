'use strict';

describe('Service: AddProduct', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var AddProduct;
  beforeEach(inject(function (_AddProduct_) {
    AddProduct = _AddProduct_;
  }));

  it('should do something', function () {
    expect(!!AddProduct).toBe(true);
  });

});
