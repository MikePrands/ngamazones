'use strict';

describe('Service: DeleteCollection', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var DeleteCollection;
  beforeEach(inject(function (_DeleteCollection_) {
    DeleteCollection = _DeleteCollection_;
  }));

  it('should do something', function () {
    expect(!!DeleteCollection).toBe(true);
  });

});
