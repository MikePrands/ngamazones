'use strict';

describe('Service: FollowUser', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var FollowUser;
  beforeEach(inject(function (_FollowUser_) {
    FollowUser = _FollowUser_;
  }));

  it('should do something', function () {
    expect(!!FollowUser).toBe(true);
  });

});
