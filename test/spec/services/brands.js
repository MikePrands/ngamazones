'use strict';

describe('Service: brands', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var brands;
  beforeEach(inject(function (_brands_) {
    brands = _brands_;
  }));

  it('should do something', function () {
    expect(!!brands).toBe(true);
  });

});
