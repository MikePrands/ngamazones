'use strict';

describe('Service: TheEnvironment', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var TheEnvironment;
  beforeEach(inject(function (_TheEnvironment_) {
    TheEnvironment = _TheEnvironment_;
  }));

  it('should do something', function () {
    expect(!!TheEnvironment).toBe(true);
  });

});
