'use strict';

describe('Service: UserCollections', function () {

  // load the service's module
  beforeEach(module('theamazonesApp'));

  // instantiate service
  var UserCollections;
  beforeEach(inject(function (_UserCollections_) {
    UserCollections = _UserCollections_;
  }));

  it('should do something', function () {
    expect(!!UserCollections).toBe(true);
  });

});
