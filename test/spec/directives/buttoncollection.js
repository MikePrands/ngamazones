'use strict';

describe('Directive: buttonCollection', function () {

  // load the directive's module
  beforeEach(module('theamazonesApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<button-collection></button-collection>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the buttonCollection directive');
  }));
});
