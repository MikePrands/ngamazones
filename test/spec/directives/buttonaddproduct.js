'use strict';

describe('Directive: buttonAddproduct', function () {

  // load the directive's module
  beforeEach(module('theamazonesApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<button-addproduct></button-addproduct>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the buttonAddproduct directive');
  }));
});
