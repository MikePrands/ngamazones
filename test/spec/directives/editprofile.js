'use strict';

describe('Directive: editProfile', function () {

  // load the directive's module
  beforeEach(module('theamazonesApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<edit-profile></edit-profile>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the editProfile directive');
  }));
});
