'use strict';

describe('Directive: addtoCollection', function () {

  // load the directive's module
  beforeEach(module('theamazonesApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<addto-collection></addto-collection>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the addtoCollection directive');
  }));
});
